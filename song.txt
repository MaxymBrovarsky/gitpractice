Loving him is like driving a new Maserati down a dead-end street
Faster than the wind, passionate as sin ending so suddenly
Loving him is like trying to change your mind once you're already flying through the free fall
Like the colors in autumn, so bright just before they lose it all
Losing him was blue like I'd never known
Missing him was dark grey all alone
Forgetting him was like trying to know somebody you never met
But loving him was red
Loving him was red
Touching him was like realizing all you ever wanted was right there in front of you
Memorizing him was as easy as knowing all the words to your old favorite song
Fighting with him was like trying to solve a crossword and realizing there's no right answer
Regretting him… 